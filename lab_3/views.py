from django.shortcuts import render
from .models import Friend
from .forms import FriendForm
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all()  # TODO Implement this
    response = {'friends': friends}
    return render(request, 'friend_list_lab3.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    context={}
    form = FriendForm(request.POST or None, request.FILES or None)

    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/lab-3')
    
    context['form']=form
    return render(request,"lab3_form.html",context)
