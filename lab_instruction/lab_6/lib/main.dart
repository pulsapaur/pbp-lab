import 'package:flutter/material.dart';
import './screens/home_screen.dart';

void main() => runApp(Mutuals());

class Mutuals extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<Mutuals> {
  Map<String, bool> _filters = {};


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Mutuals',
      theme: ThemeData(
        primarySwatch: Colors.yellow,
        canvasColor: Color.fromRGBO(255,255,255,1),
        fontFamily: 'Raleway',
        textTheme: ThemeData.light().textTheme.copyWith(
            bodyText1: TextStyle(
              color: Color.fromRGBO(20, 51, 51, 1),
            ),
            bodyText2: TextStyle(
              color: Color.fromRGBO(20, 51, 51, 1),
            ),
            headline6: TextStyle(
              fontSize: 20,
              fontFamily: 'RobotoCondensed',
              fontWeight: FontWeight.bold,
            )),
      ),
      // home: CategoriesScreen(),
      initialRoute: '/', // default is '/'
      routes: {
        '/': (ctx) => Homepagee(),
      },
      onGenerateRoute: (settings) {
        print(settings.arguments);
      },
    );
  }
}