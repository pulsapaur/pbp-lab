import 'package:flutter/material.dart';
import 'package:flutter_complete_guide/widgets/main_drawer.dart';

class Homepagee extends StatefulWidget {

  @override
  _HomepageeScreenState createState() => _HomepageeScreenState();
}

class _HomepageeScreenState extends State<Homepagee> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: false,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        
      ),
      drawer: MainDrawer(),
      body: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Column(crossAxisAlignment: CrossAxisAlignment.center, 
          children: [
            Image.asset(
              'assets/images/mutuals.png',
              width: 200,
            ),
            Padding(padding: EdgeInsets.all(100)),
            Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [                
                  Text('Welcome to Mutuals',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 32,
                        color: Color.fromRGBO(0, 0, 0, 1),
                      )),
                  Image.asset(
                    'assets/images/Hand.png',
                    width: 40,
                  )
                ]
              ),
            Text('''\nMutuals is a web application that focuses in connecting end user 
to a diverse group of people sharing the same interests. 
You may message them through the message box feature, 
or have fun with our shuffle button and communicate with 
randomly selected users based on your shared interests.\n''',
              style: TextStyle(
                fontSize: 13.0,
                color: Color.fromRGBO(0, 0, 0, 1),
              ),
              textAlign: TextAlign.center,
            ),
            Text(
              "Haven't created an account?\n",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Color.fromRGBO(0, 0, 0, 1),
              ),
              textAlign: TextAlign.center,
            ),
            ElevatedButton(
                child: Padding(
                    padding: EdgeInsets.all(10),
                    child: Text(
                      "Let's Be Mutuals",
                      style: TextStyle(
                        fontSize: 13.0,
                        fontWeight: FontWeight.bold,
                        color: Color.fromRGBO(0, 0, 0, 1),
                      ),
                    )
                ),
                onPressed: () {
                  
                }
              )
          ])
      ])
    );
  }
}