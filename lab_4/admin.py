from django.contrib import admin

from lab_4.models import Note

# Register your models here.
admin.site.register(Note)
