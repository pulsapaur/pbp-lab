from django.shortcuts import render

from lab_4.forms import NoteForm
from .models import Note
from django.http.response import HttpResponseRedirect

# Create your views here.
def index(request):
    note = Note.objects.all()
    response = {'note' : note}
    return render(request, 'lab4_index copy.html', response)

def note_list(request):
    note = Note.objects.all()
    response = {'note' : note}
    return render(request, 'card.html', response)

def add_note(request):
    context={}
    form = NoteForm(request.POST or None, request.FILES or None)

    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/lab-4')
    
    context['form']=form
    return render(request,"forms.html",context)