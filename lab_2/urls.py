from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('xml',views.xml, name='xml'),
    path('json',views.json, name='json')
]